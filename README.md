# DOCKER :whale:

Ce script vise à installer Docker ainsi que compose sur Debian 12.

## INSTALLATION ON DEBIAN 12:

```bash
git clone https://git.legaragenumerique.fr:GARAGENUM/docker-install.git
cd docker-install/
sudo ./docker-install.sh
```

or

```bash
curl -sS https://git.legaragenumerique.fr/GARAGENUM/docker-install/raw/branch/main/docker-install.sh | bash
```

## DOCUMENTATION :book:

> [DOCKER](https://docs.docker.com/engine/install/debian/)